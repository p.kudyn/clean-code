openapi: 3.0.0
info:
  description: |
    Superadmin API
  version: "1.0.0"
  title: Superadmin API
  contact:
    email: info@superkoders.com
  license:
    name: Apache 2.0
    url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
servers:
  - url: 'https://balistas.cz/api/v1'
tags:
  - name: auth
    description: Authentication/Authorization
  - name: misc
    description: Everything that does not fit any other tag :-)
  - name: order
    description: Everything about orders
  - name: product
    description: Everything about products

paths:
  /echo:
    get:
      tags:
        - misc
      summary: Echo point
      responses:
        '500':
          $ref: '#/components/responses/Error500Response'
        '200':
          $ref: '#/components/responses/SuccessResponse'
  /auth:
    post:
      tags:
        - auth
      summary: User's authentication process. Returns a JWT token to be used in all other calls.
      requestBody:
        $ref: '#/components/requestBodies/Auth'
      responses:
        '400':
          $ref: '#/components/responses/Error400Response'
        '200':
          $ref: '#/components/responses/AuthResponse'
components:
  responses:
    Error500Response:
      description: 5xx Internal server errors
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                enum: ['error']
              code:
                type: integer
                minimum: 500
                maximum: 599
                format: int32
                example: 500
              message:
                type: string
                example: "Internal server error"
    Error400Response:
      description: 4xx Errors
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                enum: [ 'error' ]
              code:
                type: integer
                minimum: 400
                maximum: 499
                format: int32
                example: 400
              message:
                type: string
                example: "Invalid object ID"
              context:
                type: object
                nullable: true
    SuccessResponse:
      description: Successful API call
      content:
        application/json:
          schema:
            type: object
            properties:
              status:
                type: string
                enum: [ 'ok' ]
              message:
                type: string
                example: "Operation performed successfully"
    AuthResponse:
      description: Authentication response
      content:
        application/json:
          schema:
            type: object
            properties:
              jwt:
                type: string
                description: JWT token
                example: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiYXBpIiwiaWF0IjoxNjM0NjI5NjYxLCJleHAiOiIxNjM1MjM0NDYxIn0.iJYe1L75nbdfp2bq8-Ujz9Nx00KeXYupHaPhUAi39ag"
              expires:
                type: string
                description: JWT token expiration datetime
                example: "2021-12-24 18:00:00"
  requestBodies:
    Auth:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ApiUser'
      required: true
  schemas:
    ApiUser:
      type: object
      description: API user, used in authentication process
      required:
        - username
        - password
      properties:
        username:
          type: string
        password:
          type: string
  securitySchemes:
    superadminAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
